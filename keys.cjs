function keys(obj) {


    if (!typeof (obj) === 'object' || typeof (obj) === 'string' || obj === null || obj === undefined) {
        return [];
    }
    const Keys=[];
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            Keys.push(key);
        }
    }
    return Keys;
}

module.exports = keys;
