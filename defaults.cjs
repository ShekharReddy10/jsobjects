function defaults(obj, defaultProps) {
    if (!typeof (obj) === 'object' || typeof (obj) === 'string' || obj === null || obj === undefined) {
        return [];
    }
    let defaults = {};
    for (let key in obj) {
        if (defaultProps[key]) {
            defaults[key] = defaultProps[key];
        }
        else {
            defaults[key] = obj[key];
        }
    }
    for (let key in defaultProps) {
        if (obj[key]) {
            defaults[key] = obj[key];
        }
        else {
            defaults[key] = defaultProps[key];
        }
    }
    return defaults
}
module.exports = defaults;
