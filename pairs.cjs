const keys = require('./keys.cjs');
const values = require('./values.cjs');
function pairs(obj) {
    const Pairs = [];
    if (!typeof (obj) === 'object' || typeof (obj) === 'string' || obj === null || obj === undefined){
        return [];
    }
    const Keys = keys(obj);
    const Values = values(obj);
    for (let i = 0; i<Keys.length ;i++) {       
        Pairs.push([Keys[i] , Values[i]]);
    }
    return Pairs;
}
module.exports = pairs;