const keys = require('./keys.cjs');
const values = require('./values.cjs');
function invert(obj) {
    if (!typeof (obj) === 'object' || typeof (obj) === 'string' || obj === null || obj === undefined){
        return [];
    }
    const Key = keys(obj);
    const Values = values(obj);
    const invert = []
    for (let i = 0; i < Key.length; i++) {
        invert.push([Values[i], Key[i]]);
    };
    return invert;
}
module.exports = invert;