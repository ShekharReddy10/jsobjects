function mapObject(obj, cb) {
    const mapped = {}
    if (!typeof (obj) === 'object' || typeof (obj) === 'string') {
        return [];
    }
    for (let index in obj) {
        mapped[index] = cb(obj[index]);
    }
    return mapped;
}
module.exports = mapObject;